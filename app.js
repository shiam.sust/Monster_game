new Vue({
	el: '#app',
	data: {
		playerHealth: 100,
		monsterHealth: 100,
		gameIsRunnig: false,
		turns: [],
		specialAttackTimes: 1,
		healTimes: 1
	},
	methods: {
		startGame: function(){
			this.gameIsRunnig = true;
			this.playerHealth = 100;
			this.monsterHealth = 100;
			this.specialAttackTimes= 1;
			this.healTimes = 1;
			this.turns = [];
		},
		attack: function(){
			damage = this.calculateDamage(10,3);
			this.monsterHealth -= damage;
			this.turns.unshift({
				isPlayer: true,
				text: 'player hits monster for ' + damage + ' damage'
			});
			if(this.checkWin()){
				return;
			}
		
			this.monsterAttack();
			this.checkWin();

		},
		specialAttack: function(){
			damage = this.calculateDamage(10,20);
			this.monsterHealth -= damage;
			this.turns.unshift({
				isPlayer: true,
				text: 'player hits heavily monster for ' + damage + ' damage'
			});
			this.specialAttackTimes = 0;
			if(this.checkWin()){
				return;
			}
			this.monsterAttack();
		},
		heal: function(){
			if(this.playerHealth <= 90){
				this.playerHealth += 10;
			}else{
				this.playerHealth = 100;
			}
			this.turns.unshift({
				isPlayer: true,
				text: 'player is healed 10 units'
			});
			this.healTimes = 0; 
			this.monsterAttack();
		},
		giveUp: function(){
			this.gameIsRunnig = false;
		},
		monsterAttack: function(){
			damage = this.calculateDamage(12,4);
			this.playerHealth -= damage;
			this.turns.unshift({
				isPlayer: false,
				text: 'monster hits for ' + damage + ' damage'
			});
		},
		calculateDamage: function(max, min){
			return Math.max(Math.floor(Math.random() * max) + 1, min);
		},
		checkWin: function(){
			if(this.playerHealth <= 0){
				if(confirm('you lost! play again?')){
					this.startGame();
				}else{
					this.gameIsRunnig = false;
				}
				return true;
			}
			else if(this.monsterHealth <= 0){
				if(confirm('you win! play again?')){
					this.startGame();
				}else{
					this.gameIsRunnig = false;
				}
				return true;
			}
			return false;
		}
	}
});